# Working session : developping a REST web service with Spring


1/ Make the first test pass (helloworld test), by implementing the controller (see here if needed => https://spring.io/guides/gs/rest-service/)

- have a look at the test, how to read it ?
- implement the controller, by using Spring annotations (@RestController, @RequestMapping)

2/ Make the second test pass, getCustomersTest.
Tips : Return a collection of "Customers", hard code the data
Once the test is OK, run your project, test it with postman
- how does the json is generated ? 

3/ Make the "getCustomerById" test pass
Use Spring annotation @PathVariable 
Run & Test with Postman

4/ Make the postCustomerTest pass
- Use Spring annotation @RequestBody & @ResponseStatus
- do not implement the logic (the service store nothing, it does not impact the results of other services)
- how does the json is translated to Java ?
- which status code should be returned ?
Run & Test with Postman

5/ Error handling : make the "getCustomerByIdThrowsException" pass
- use one of the 3 approaches here : https://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc
Run & test with Postman

6/ Make the getCustomerByName test pass 
Run & test with Postman


7/ Implement the business logic of the application
- Create a "CustomerService.java" interface, with methods allowing creation & search of customers
- Create an implementation for this interface, for example using a HashMap
- Update your existing controller
- Rq : the tests should be broken - use @Ignore to desactivate them

8/ implement a PUT operation (update an existing customer)
9/ implement a DELETE operation

10/ implement XML binding : depending on the Accept header provided in the request, return a json response, or a XML response

11/ Change the name of the fields generated in the response - without impacting the code
- firstName becomes givenName
- lastName becomes familyName
- fix the order to have : id, then familyName, then firstName
- do this implementation for both json and xml
Tips : for json,  add jackson annotations in the Customer.java class. For xml, add JAXB annotation in the DTO class

12/ implement the 3 possible approaches for exception handling

Bonus/
 - Livecoding : see how to integrate swagger-ui
Show swagger-ui at http://localhost:8081/swagger-ui.html
