package com.acme.customers.server.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Repository;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(includeFilters = @ComponentScan.Filter(Repository.class))
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
// @formatter:off
public class CustomerApiUnitTest {

	@Autowired
	private MockMvc mvc;

	@Test
	public void helloWorldTest() throws Exception {
		this.mvc.perform(get("/hello")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
				.andExpect(content().string("Hello"));
	}

	@Test
	public void getCustomersTest() throws Exception {

		this.mvc.perform(get("/customers").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.length()").value(3))
				.andExpect(jsonPath("$[?(@.id==1)].firstName").value("Roy"))
				.andExpect(jsonPath("$[?(@.id==1)].lastName").value("Fielding"))
				.andExpect(jsonPath("$[?(@.id==2)].firstName").value("James"))
				.andExpect(jsonPath("$[?(@.id==2)].lastName").value("Gosling"))
				.andExpect(jsonPath("$[?(@.id==3)].firstName").value("Rod"))
				.andExpect(jsonPath("$[?(@.id==3)].lastName").value("Johnson"));

	}

	@Test
	public void getCustomerById() throws Exception {
		this.mvc.perform(get("/customers/{customerId}", 1)
                .accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("firstName").value("Roy"))
                .andExpect(jsonPath("lastName").value("Fielding"));
	}

	@Test
	public void postCustomer() throws Exception {
		String body = "{\n" + "\n" + "    \"firstName\": \"Jean\",\n" + "    \"lastName\": \"Dupont\"\n" + "\n" + "}";

		this.mvc.perform(post("/customers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(body)
				.accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());
	}

    @Test
    public void getCustomerWithIdEqualsTo1001ThrowsException() throws Exception {
        this.mvc.perform(get("/customers/{customerId}", 1001)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
//                .andExpect(jsonPath("message").value("Customer Id should not be greater than 1000"));
    }
    
    @Test
    public void getCustomerByIdThrowsNotFoundException() throws Exception {
        this.mvc.perform(get("/customers/{customerId}", 0)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }    

    @Test @Ignore
    public void getCustomerWithNegativeIdThrowsException() throws Exception {
        this.mvc.perform(get("/customers/{customerId}", -1)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("code").value(4))
                .andExpect(jsonPath("message").value("Customer id should not be negative"));
    }
    
    @Test @Ignore
    public void getCustomersByName() throws Exception {
        this.mvc.perform(get("/customers?firstName=Roy")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(1))
                .andExpect(jsonPath("$[?(@.id==1)].firstName").value("Roy"))
                .andExpect(jsonPath("$[?(@.id==1)].lastName").value("Fielding"));
    }

}
