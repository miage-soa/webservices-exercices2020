package com.acme.customers.server.repository;

import com.acme.customers.server.repository.model.CustomerDO;

import org.springframework.context.annotation.Profile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
@Profile("spring-data")
public interface CustomerCRUDRepository extends CrudRepository<CustomerDO, Long>, CustomerRepository {

}
