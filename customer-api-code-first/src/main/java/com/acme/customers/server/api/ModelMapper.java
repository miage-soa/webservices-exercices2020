package com.acme.customers.server.api;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import com.acme.customers.server.api.model.Customer;
import com.acme.customers.server.repository.model.CustomerDO;

public class ModelMapper {

    public static List<Customer> mapToCustomerList(Iterable<CustomerDO> customerDOs) {
        return StreamSupport.stream(customerDOs.spliterator(), false).map(ModelMapper::mapToCustomer)
                .collect(Collectors.toList());
    }

    public static Customer mapToCustomer(CustomerDO customerDO) {
        return new Customer(customerDO.getId(), customerDO.getFirstName(), customerDO.getLastName());
    }

    public static CustomerDO mapToCustomerDO(Customer customer) {
        return new CustomerDO(customer.getId(), customer.getFirstName(), customer.getLastName());
    }

}
