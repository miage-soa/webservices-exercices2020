package com.acme.customers.server.repository;

import java.util.Collection;

import com.acme.customers.server.repository.model.CustomerDO;
import com.acme.customers.server.repository.model.CustomerNotFoundBusinessException;

public interface CustomerRepository {
    Collection<CustomerDO> findAll();

    CustomerDO findOne(long id) throws CustomerNotFoundBusinessException;

    CustomerDO save(CustomerDO customerDO);

    Collection<CustomerDO> findAllByFirstName(String firstName);

    void deleteById(long customerId);
}
