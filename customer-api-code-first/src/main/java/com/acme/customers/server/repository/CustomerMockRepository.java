package com.acme.customers.server.repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import com.acme.customers.server.repository.model.CustomerDO;
import com.acme.customers.server.repository.model.CustomerNotFoundBusinessException;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

@Repository
@Profile("!spring-data")
public class CustomerMockRepository implements CustomerRepository {

    private Map<Long, CustomerDO> customers = new HashMap();

    public CustomerMockRepository() {
        customers.put(1L, new CustomerDO(1, "Roy", "Fielding"));
        customers.put(2L, new CustomerDO(2, "James", "Gosling"));
        customers.put(3L, new CustomerDO(3, "Rod", "Johnson"));
    }


    @Override
    public Collection<CustomerDO> findAll() {
        return customers.values();
    }

    @Override
    public CustomerDO findOne(long id) {
        if (customers.containsKey(id))
            return customers.get(id);
        else
            throw new CustomerNotFoundBusinessException();
    }

    @Override
    public CustomerDO save(CustomerDO customerDO) {
//        Long customerId = customers.keySet().stream().max(Comparator.naturalOrder()).get() + 1;
        customers.put(customerDO.getId(), customerDO);
        return customerDO;
    }

    @Override
    public Collection<CustomerDO> findAllByFirstName(String firstName) {
        return customers.values().stream().filter(c -> c.getFirstName().equals(firstName)).collect(Collectors.toList());
    }

    @Override
    public void deleteById(long customerId) {
        customers.remove(customerId);
    }
}
