package com.acme.customers.server.api;

import static com.acme.customers.server.api.ModelMapper.mapToCustomer;
import static com.acme.customers.server.api.ModelMapper.mapToCustomerDO;
import static com.acme.customers.server.api.ModelMapper.mapToCustomerList;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.acme.customers.server.api.model.Customer;
import com.acme.customers.server.api.model.ExceptionInfo;
import com.acme.customers.server.repository.CustomerRepository;
import com.acme.customers.server.repository.model.CustomerDO;
import com.acme.customers.server.repository.model.CustomerNotFoundBusinessException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerApi {

    private CustomerRepository customerRepository;

    @Autowired
    public CustomerApi(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/hello")
    public String helloWorld() {
        return "Hello";
    }


    @RequestMapping(method = RequestMethod.GET, path = "/customers")
    public List<Customer> getCustomers(@RequestParam(value = "firstName", required = false) String firstName) {
        if (firstName != null && !"".equals(firstName))
            return mapToCustomerList(customerRepository.findAllByFirstName(firstName));
        else
            return mapToCustomerList(customerRepository.findAll());

    }

    @RequestMapping(method = RequestMethod.GET, path = "/customers/{customerId}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable int customerId) {
        if (customerId > 1000) {
            throw new CustomerIdOutOfRangeException("CustomerId cannot be greater than 1000");
        }
        if (customerId < 0) {
            throw new IllegalArgumentException("Customer id should not be negative");
        }
        CustomerDO customerDO = customerRepository.findOne(customerId);
        Customer customer = mapToCustomer(customerDO);
        return ResponseEntity.ok(customer);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/customers")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void postCustomer(@RequestBody @Valid Customer customer) {
        customerRepository.save(mapToCustomerDO(customer));
    }


    @RequestMapping(method = RequestMethod.DELETE, path = "/customers/{customerId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable int customerId) {
        customerRepository.deleteById(customerId);
    }


    @ExceptionHandler(CustomerNotFoundBusinessException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    ExceptionInfo handleBadRequest(HttpServletRequest req, Exception ex) {
        return new ExceptionInfo(1, "Customer not found", "url");
    }

}