/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.customers.server.api;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author lyad7461
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Customer Id should not be greater than 1000")
public class CustomerIdOutOfRangeException extends RuntimeException {

    public CustomerIdOutOfRangeException(String message) {
        super(message);
    }
}
