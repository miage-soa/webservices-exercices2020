package com.acme.customers.controller.api;

import static java.util.Arrays.asList;
import static java.util.Objects.isNull;

import java.util.List;

import javax.validation.Valid;

import com.acmee.customers.controller.api.CustomersApi;
import com.acmee.customers.controller.model.Address;
import com.acmee.customers.controller.model.Customer;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class CustomersController implements CustomersApi {

    @Override
    public ResponseEntity<List<Customer>> getCustomersByCriteria(@RequestParam(value = "firstName", required = false) String firstName, @RequestParam(value = "mobilePhone", required = false) String mobilePhone) {
        // hard coded behaviour. In real life, we should call here other logic, probably connected to a database
        if (isNull(mobilePhone) || mobilePhone.isEmpty()) {
            return ResponseEntity.ok().build();
        }
        else if ("0606060606".equals(mobilePhone)) {
            return ResponseEntity.ok(asList(createCustomer("Roy", "Fiedling", "M.", "roy.fiedling@acmee.com", "0606060606", "0505050505", "Toulouse", 31000)));
        }
        else if ("0607070707".equals(mobilePhone)) {
            return ResponseEntity.ok(asList(createCustomer("Cécile", "Berteau", "Mrs.", "cecile.berteau@acmee.com", "0607070707", "05007070707", "Paris", 75001)));
        }
        else {
            return ResponseEntity.ok().build();
        }
    }

    private Customer createCustomer(String firstName, String lastName, String salutation, String email, String mobilePhone, String fixPhone, String city, int zipCode) {
        Customer customer = new Customer();
        customer.setId(1L);
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setSalutation(salutation);
        customer.setEmail(email);
        customer.setMobilePhone(mobilePhone);
        customer.setFixPhone(fixPhone);
        Address address = new Address();
        address.setCity(city);
        address.setZipCode(zipCode);
        customer.setAdresses(asList(address));
        return customer;
    }


    @Override
    public ResponseEntity<Customer> getCustomerById(@PathVariable Integer customerId) {
        if (customerId > 1000) {
            throw new CustomerIdOutOfRangeException("CustomerId cannot be greater than 1000");
        }
        if (customerId < 0) {
            throw new IllegalArgumentException("Customer id should not be negative");
        }
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<Void> postCustomer(@RequestBody @Valid Customer customer) {
        return ResponseEntity.noContent().build();
    }


    @Override
    public ResponseEntity<Void> deleteCustomerById(@PathVariable("customerId") Integer customerId) {
        return ResponseEntity.noContent().build();
    }

}