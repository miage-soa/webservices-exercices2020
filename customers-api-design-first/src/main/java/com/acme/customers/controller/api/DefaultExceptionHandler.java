/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.acme.customers.controller.api;

import javax.servlet.http.HttpServletRequest;

import com.acmee.customers.controller.model.RestError;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 * @author lyad7461
 */
@RestControllerAdvice
public class DefaultExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public RestError defaultErrorHandler(HttpServletRequest req, Exception e) {
        RestError restError = new RestError();
        restError.setCode(4);
        restError.setMessage(e.getMessage());
        return restError;

    }
}
    

