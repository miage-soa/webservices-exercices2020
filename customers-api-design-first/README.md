# Working session : developping REST Web service with swagger & a design first approach

## Context

We will use the Open API Specficiation  3.0, and swagger-editor tools to write an API specification

Usefull links :

* The basics for the specification : https://swagger.io/docs/specification/basic-structure/
* the full Open API Specification : https://github.com/OAI/OpenAPI-Specification
* swagger-editor : https://editor.swagger.io/ (or [here](https://swagger.io/swagger-editor/)

## The customer API v1

Describe an API in order to manage the "Customer" ressource.

A customer has the following fields :

* id : integer, mandatory, id of the ressource
* firstname : string, mandatory
* lastname : string, mandatory

v1.a) Use swagger editor to specify the REST operation needed to get a customer by its id 

v1.b) Add an operation in order to create a customer

v1.c) Add the operation : delete a customer by its ID

v1.d) Add one operation in order to find all customers / or find customers by firstName / or find customers by lastName

## The customer API v2

Enrich the data model, and use checks in the specification to validate fields values

v2.a) Add fields on the customer ressource

* email : string, optional, if present, should match with valid email format
* list of addresses : object (see below). A customer has at least one address, and can have several ones

An address has the following fields :

- zip code : integer, 5 digits
- city : string

## The customer API v3

Add error definitions in the spec.

v3.a) Which operations could return an http error with code 404 ?

v3.b) Update the spec to add this error as a possible response.

An error should have the following fields :

* code : integer, mandatory, between 100 and 999
* message : string, less than 255 chars

## Java/Spring server side integration

a\ Use swagger editor generation capabilities to generate the server code (choose spring type).
Compile it : `mvn clean install` and run it, `mvn spring-boot:run`
Take care to change before running the port number (use 8081), in application.properties

Test the url http://localhost:8081/v1

Try some operation with the "Try it out" button.
What happens if you provide an incorrect email ? what happens if you provide no address ?
Why ? (look at the generated code)

b\ Live coding : example of how we could use swagger codegen in real life to automate interfaces generation




