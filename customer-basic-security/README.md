# Spring Security : Basic Authentication and Authorization using spring boot

## What is Basic Authentication
Basic authentification is a standard HTTP header with the user and password encoded in base64 : __Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==__.The __userName__ and __password__ is encoded in the format __username:password__. This is one of the simplest technique to protect the REST resources because it does not require cookies. session identifiers or any login pages.

In case of basic authentication, the username and password is only encoded with Base64, but not encrypted or hashed in any way. Hence, it can be compromised by any man in the middle. Hence, it is always recommended to authenticate rest API calls by this header over a ssl connection.

## BasicAuthenticationFilter in Spring
BasicAuthenticationFilter in Spring is the class which is responsible for processing basic authentication credentials presented in HTTP Headers and putting the result into the SecurityContextHolder. The standard governing HTTP Basic Authentication is defined by RFC 1945, Section 11, and BasicAuthenticationFilter confirms with this RFC.

## Step 1 : Init project
* 1- git clone https://framagit.org/miage-soa/webservices-exercices2020
* 2- open project with netbeans
* 3- clean and build

# Step 2 : Test the default spring boot security
* 1- open the main class
* 2- Enable the web security by adding the __@EnableWebSecurity__ annotation
```
  @SpringBootApplication
  @EnableWebSecurity
  public class SpringBasicSecurityApplication {

  	public static void main(String[] args) {
  		SpringApplication.run(SpringBasicSecurityApplication.class, args);
  	}

  }
```
* 3- Add the application controller

  - create a new java package : com.miage.security.api.controller
  - create java class  ApplicationController

  ```
  package com.miage.security.api.controller;
  public class ApplicationController {

  	public String greeting() {
  		return "spring security example";
  	}

  }
  ```
  - add __@RestController__  
  - define an entry point __(/rest/auth)__ for the controller : use the __@RequestMapping__ annotation
  - add __@GetMapping("/getMsg")__ to the greetting function


* 4- Add the spring security parameters as spring properties
  - open application.properties located at the resources folder
  - define a user name __spring.security.user.name=miage__
  - deine a new passord : __spring.security.user.password=password__


* 5- Run the project as java Application
* 6- Test the endpoint with postman :
  - send a get request http://localhost:8080/rest/auth/getMsg
* 7- Explain what happen ?
* 8- Add the authentication parmameters to the Authorization field
* 9- Re test again

## Step 3 : Spring security configuraiton based on URL
In this step we will test the spring security based on URL. Security mechanism will be applied only for a given route. To do this, we create a new Rest Controller __NoAuthController__ with entry point __('/rest/noAuth/')__, we configure our service by adding a simple gretting function eg.__sayHi()__ and then we specify a new security rule in (SpringSecurityConfig service) in order to secure only routes starting by __/rest/auth/**__

* 1- create a new rest controller __NoAuthController__ with request mapping for __/noAuth/rest__
* 2- add a simple gretting function
```
  @GetMapping("/sayHi")
	public String sayHi() {
		return "hi";
	}
```
* 3- Create a new package : __com.miage.security.api.config__
* 4- Add a java class : __SpringSecurityConfig__

```
  package com.miage.security.api.config;

  import org.springframework.context.annotation.Bean;
  import org.springframework.context.annotation.Configuration;
  import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
  import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
  import org.springframework.security.config.annotation.web.builders.HttpSecurity;
  import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
  import org.springframework.security.config.http.SessionCreationPolicy;
  import org.springframework.security.crypto.password.NoOpPasswordEncoder;

  @SuppressWarnings("deprecation")
  @Configuration
  @EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
  public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {


    //users configuration
  	@Override
  	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
  		auth.inMemoryAuthentication().withUser("miage").password("password").roles("USER");
  	}



    //--------------------------------------------
    // Rule 1 : security for all API
    //--------------------------------------------
    @Override protected void configure(HttpSecurity http) throws Exception {    
      http.httpBasic().
        realmName("spring-app").
        and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
        and().csrf().disable().
        authorizeRequests().anyRequest().fullyAuthenticated().and().
        httpBasic();
    }

    //--------------------------------------------
  	// Rule 2 : security based on URL
    //--------------------------------------------
      /*
      * add code herer
      */


    //--------------------------------------------
  	// Rule 3 : security based on ROLE
    //--------------------------------------------
      /*
      * add code herer
      */  


    @Bean
    public NoOpPasswordEncoder passwordEncoder() {
        return (NoOpPasswordEncoder) NoOpPasswordEncoder.getInstance();
    }
  }
```  

* 5- Comment the Rule 1 and define the Rule 2 as follow : only route start by __/rest/auth**__ must be secured
```
@Override
        protected void configure(HttpSecurity http) throws Exception {
            http.httpBasic().
                    realmName("spring-app").
                    and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                    and().csrf().disable().
                    authorizeRequests().antMatchers("/rest/auth/**").fullyAuthenticated().and().httpBasic();
        }
```
* 6- test the security rules with postman
 - T1 : http://localhost:8080/noAuth/rest/sayHi with Autorisation = noAuth
 - T2 : http://localhost:8080/rest/auth/getMsg with Autorisation = { username : miage, password : password}

* 7- Explain what happened ?  

## Step 4 : Spring security configuration based on user role
in this step we will test spring security based on user role. we insert a new user name admin with role ADMIN, then we configure a new rule to allow only ADMIN user to reach '__/rest/auth/**__'  

* 1- add a new user Admin, password admin and role Admin in the spring security config
* 2- comment Rule 2 
* 3- add Rule 3 as follow
```
@Override
	protected void configure(HttpSecurity http) throws Exception {
		 http.httpBasic().
                    realmName("spring-app").
                    and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).
                    and().csrf().disable().
                    authorizeRequests().antMatchers("/rest/**").hasAnyRole("ADMIN").anyRequest().fullyAuthenticated().and()
				.httpBasic();
	}
```
* 6- test the Rule 3 with postman
 - T1 : http://localhost:8080/rest/auth/getMsg with Autorisation = { username : miage, password : password}
 - T2 : http://localhost:8080/rest/auth/getMsg with Autorisation = { username : admin, password : admin}

* 7- Explain what happened ? What is code error ?


## Step 5 : Testing Role based Authorization with spring security

* 1- create a new package : __com.miage.security.api.model__
* 2- create a new java class as __CustomerProfile__ model

```
package com.miage.security.api.model;

public class CustomerProfile {

    private String username;

    private String name;

    public CustomerProfile() {
        super();
    }

    public CustomerProfile(String username, String name) {
        this.name = name;
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

```

* 3- create a __CustomerControler__ within the controllers package
```

package com.miage.security.api.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import com.miage.security.api.model.CustomerProfile;

@RestController 
public class CustomerController {

    @Secured("ROLE_USER")
    @GetMapping(value = "/api/v0/user")
    public String welcomeAppUser(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @Secured({"ROLE_USER", "ROLE_ADMIN" })
    @GetMapping(value = "/api/v1/user")
    public String welcomeAppUser4(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }


    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/api/v2/user")
    public String welcomeAppUser1(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }


    @PreAuthorize("hasRole('USER') AND hasRole('ADMIN')")
    @GetMapping(value = "/api/v3/user")
    public String welcomeAppUser2(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @PreAuthorize("hasRole('USER') OR hasRole('ADMIN')")
    @GetMapping(value = "/api/v4/user")
    public String welcomeAppUser3(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @PreAuthorize("hasRole('USER')")
    @GetMapping(value = "/api/v5/user")
    public CustomerProfile welcomeAppUser5(@AuthenticationPrincipal User user) {
        return new CustomerProfile("Miage", "TOULOUSE");
    }


    @RolesAllowed("ADMIN")
    @GetMapping(value = "/api/v6/user")
    public String welcomeAppUser6(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }


    @RolesAllowed({"USER", "ADMIN"})
    @GetMapping(value = "/api/v7/user")
    public String welcomeAppUser7(@AuthenticationPrincipal User user) {
        return "Welcome User " + user.getUsername();
    }

    @GetMapping(value = "/rest/user")
    public CustomerProfile guestUser() {
        return new CustomerProfile("carthage","123");
    }
}

```
* 4- Explore the annotations and Role based authorisation usage
* 5- test all endpoints with postman, 
* 6- NOTE : student must explain and show all tests to the professor 


# Task 2 : Secure the customer-api-code-first
* 1- run and build the customer-api-code-first [link](https://framagit.org/miage-soa/webservices-exercices2020/-/tree/master/customer-api-code-first)
* 2- integrate spring security package
* 3- Create two users with two differents roles ( USER and ADMIN)
* 4- Add security rules :
- All routes must be secured
- create and update and delete functions must be granted only to ADMIN




### Guides
The following guides illustrates how to use certain features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)