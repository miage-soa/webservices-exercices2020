# "Customer View" API consumer exercise

Objective : understand the journey of API consumers, and be able to implement API clients

## Pre requisite

- pre requisite : execute the customer api server, by running the jar file "java -jar customers-api-server.jar"
- the customer api should then be accessible on http://localhost:8082

## Step 1 : Request the customer API and show the result in the web page

* make a request to the customer API to get and display customer fields in the web app, according to the selected phone number
* look at existing code : index.html and controller.js
* complete the controller.js file in order to call the customer API and fullfill web page appropriated fields

## Step 2 : Request the Orange Store Locator API and show the result in the web page

* go to https://developer.orange.com/apis/store , read the documentation of the store locator API
* subscribe to this API, and get a token
* update controller.js file in order to request the store locator API using the zipCode of the customer city
* display store information in the web page

## Step 3 : Integrate Google API Widget

* go to https://developers.google.com/maps/documentation/javascript/tutorial
* try to integrate in the web page a google map with a marker on the store
* update index.html and controller.js


