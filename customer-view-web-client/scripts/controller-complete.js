var map;
var publickey = "vide"

// called when click on sign-in button
$('#changerClient').click(function(e) {
    publickey = $( "#idclient").val();
    initPage();
});

// STEP 1
// Call the customer api to find customers by mobilePhone, and set variables
function initPage() {
    $.ajax({
// run the customer api on your pc
// use the jar in this directory, and execute java -jar customers-api-server.jar
        // TODO : fix the url to call, using publickey as a param
        url: "http://localhost:8082/customers?mobilePhone=" +  publickey        
    }).then(function(data) {
        // here we pick the salutation value in the json boody of the response
        $('.salutation').html(data[0].salutation);
        // TODO : do the same with firstName, lastName, fullName, email, mobilePhone and fixPhone
        $('.fullName').html(data[0].salutation + " " + data[0].firstName + " " + data[0].lastName);
        $('.email').html(data[0].email);
        $('.mobilePhone').html(data[0].mobilePhone);
        $('.fixPhone').html(data[0].fixPhone);
        $('.city').html(data[0].adresses[0].zipCode + " " + data[0].adresses[0].city);
        // TODO : call the initStore function with appropriate params
        initStore(data[0].adresses[0].zipCode);
    });
}

// STEP 2
// Call the orange store locator api
function initStore(zipCode) {
    $.ajax({
        // search orange store by zipcode
        // use the API Orange Store Locator
        // see https://developer.orange.com/apis/store/
        // TODO : fix the url to call using zipCode as a param
        url: "https://api.orange.com/poi/v1/shops?postalCode=" + zipCode,
        // Authorization key
        // See https://developer.orange.com/tech_guide/2-legged-oauth/
        // TODO : generate and set the Authorization token
        headers: { "Authorization": "Bearer yi7wqOoEQ1RGFpIGy26XRe6KlEnV"}
    }).then(function(data) {
        // TODO : set html fields for storeName, storeAddress, Zipcode, and city
        $('.storeName').html(data[0].name);
        $('.storeAddress').html(data[0].address1);
        $('.storeZipCode').html(data[0].postalCode);
        $('.storeCity').html(data[0].city);
        // TODO : call the initMap function with appropriate params
        initMap(data[0].longitude, data[0].latitude, data[0].name);
    });         
    
}


// STEP 3
// Insert google map widget
function initMap(longitude, latitude, storeName) {
// google map javascript api
// see https://developers.google.com/maps/documentation/javascript/tutorial -->
// TODO : insert code for google map widget generation
    map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: latitude, lng: longitude},
    zoom: 16
    });

// TODO : insert a marker
// see https://developers.google.com/maps/documentation/javascript/markers
  var marker = new google.maps.Marker({
    position: {lat: latitude, lng: longitude},
    map: map,
    title: storeName
  });
    
}


